import { Component, OnInit, Input, ViewChild } from '@angular/core';

import { SpacexLaunchService } from '../spacex-launch.service';

@Component({
  selector: 'app-spacex-launch-overview',
  templateUrl: './spacex-launch-overview.component.html',
  styleUrls: ['./spacex-launch-overview.component.css']
})
export class SpacexLaunchOverviewComponent implements OnInit {

  @Input() private id;
  @ViewChild('paginator') private paginator;
  launches: any[];
  tblLaunches: any[];
  displayedColumns = ['flight_number', 'rocket_name', 'rocket_type', 'launch_success', 'more'];

  constructor(private launchServ: SpacexLaunchService) { }

  ngOnInit() {
    this.launchServ.fetchAll().subscribe(l => {
      this.launches = l.reverse();
      this.paginationUpdate({
        pageIndex: 0,
        pageSize: 10
      });
    });
  }

  paginationUpdate(evnt) {
    this.tblLaunches = [];
    for (var i = evnt.pageIndex * evnt.pageSize; i < (evnt.pageIndex + 1) * evnt.pageSize; i++) {
      if (i in this.launches) {
        this.tblLaunches.push(this.launches[i]);
      }
    }
  }

}
