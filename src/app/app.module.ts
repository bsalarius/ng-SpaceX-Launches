import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { MaterialComponentsModule } from './material-components.module';
import { AppComponent } from './app.component';
import { SpacexLaunchService } from './spacex-launch.service';
import { SpacexLaunchDetailComponent } from './spacex-launch-detail/spacex-launch-detail.component';
import { SpacexLaunchOverviewComponent } from './spacex-launch-overview/spacex-launch-overview.component';
import { AppRoutingModule } from './app-routing.module';


@NgModule({
  declarations: [
    AppComponent,
    SpacexLaunchDetailComponent,
    SpacexLaunchOverviewComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    BrowserAnimationsModule,
    MaterialComponentsModule,
    AppRoutingModule
  ],
  providers: [
    SpacexLaunchService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
