import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { SpacexLaunchService } from '../spacex-launch.service';

@Component({
  selector: 'app-spacex-launch-detail',
  templateUrl: './spacex-launch-detail.component.html',
  styleUrls: ['./spacex-launch-detail.component.css']
})
export class SpacexLaunchDetailComponent implements OnInit {

  element: any = null;

  constructor(
    private route: ActivatedRoute,
    private launchServ: SpacexLaunchService,
    private location: Location
  ) { }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.launchServ.fetch(id).subscribe(l => {
      this.element = l;
      console.log(l);
    });
  }

}
